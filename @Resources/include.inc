;HWiNFO
Color1=af151f
Color4=ffffffee
Color2=ffffff99

[Strings]
FontFace=Rubik Light
InlineSetting=Italic
DynamicVariables=1
FontColor=ffffff99
Antialias=1
FontSize=42
;StringEffect=Shadow
;FontEffectColor=999999

[MusicArtist]
DynamicVariables=1
FontFace=Rounded M+ 1p light
FontSize=22
FontColor=c92e3899
StringAlign=Right
X=+1r
Y=30
Antialias=1


[MusicTrack]
FontFace=Rounded M+ 1p light
DynamicVariables=1
FontColor=ffffff99
InlineSetting=Italic
Antialias=1
FontSize=27
Y=-9r
X=([MeasureXPos]-5)
StringAlign=Right
